OS=$(shell uname -s)

help: ## show this help
	@grep -h -E '^[\.a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
		| sort \
		| awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%s\033[0m:%s\n", $$1, $$2}' \
		| column -s ':' -t

fmt: ## format source
	find . -name '*.go' \
	| fgrep -v '/vendor/' \
	| xargs goimports -w

generate: ## generate source code
	go generate ./...

lint: ## execute source linters
	golangci-lint run ./...

setup: setup/$(OS) $(SETUP) ## install tooling
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $$(go env GOPATH)/bin v1.41.1
	go install github.com/UltiRequiem/yamlfmt@latest
	go install golang.org/x/tools/cmd/goimports@latest
	go install mvdan.cc/sh/v3/cmd/shfmt@latest

setup/Linux:

upgrade: ## upgrade dependencies
	go get -u -t all

vendor: ## vendor dependencies
	go mod tidy -go=1.17 -compat=1.17
	go mod vendor
	go mod verify

barcogen:
	go build ./cmd/barcogen

.PHONY: \
	fmt \
	generate \
	help \
	lint \
	setup \
	upgrade \
	vendor