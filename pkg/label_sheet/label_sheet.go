package label_sheet

import (
	"fmt"
)

type LabelSheet struct {
	Name                   string
	PageWidth              float64 // Overall Width, e.g. 8.5"
	PageHeight             float64 // Overall Height, e.g. 11"
	PageColumns            int     // Quantity of label columns
	PageRows               int     // Quantity of label rows
	PageMarginLeft         float64 // Distance from the left edge to the first label
	PageMarginRight        float64 // Distance from the right edge to the last label
	PageMarginTop          float64 // Distance from the top edge to the first label
	LabelWidth             float64 // Label Width, e.g. 1"
	LabelHeight            float64 // Label Height, e.g. 0.5"
	LabelMarginHorizontal  float64 // Horizontal distance between labels
	LabelMarginVertical    float64 // Vertical distance between labels
	LabelPaddingHorizontal float64 // Horizontal distance between label edge and content
	LabelPaddingVertical   float64 // Vertical distance between label edge and content
}

func (ls LabelSheet) Validate() error {
	// Horizontal first.

	remaining := ls.PageWidth
	remaining -= ls.LabelWidth * float64(ls.PageColumns)
	remaining -= ls.PageMarginLeft
	remaining -= ls.PageMarginRight
	remaining -= ls.LabelMarginHorizontal * float64(ls.PageColumns-1)

	if remaining != 0 {
		return fmt.Errorf("horizontal dimensions incongruent by %f\"", remaining)
	}

	// Now vertical.

	remaining = ls.PageHeight
	remaining -= ls.LabelHeight * float64(ls.PageRows)
	remaining -= ls.PageMarginTop * 2 // assume margins are the same
	remaining -= ls.LabelMarginVertical * float64(ls.PageColumns-1)

	if remaining != 0 {
		return fmt.Errorf("vertical dimensions incongruent by %f\"", remaining)
	}

	return nil
}

var AllSheets = []LabelSheet{
	Avery61527,
}
