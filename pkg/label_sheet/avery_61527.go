package label_sheet

var Avery61527 LabelSheet = LabelSheet{
	PageWidth:              8.5,
	PageHeight:             11,
	LabelWidth:             1,
	LabelHeight:            0.5,
	PageMarginLeft:         0.40625, // 13/32
	PageMarginRight:        0.5,
	PageMarginTop:          0.5,
	LabelMarginVertical:    0.225,
	LabelMarginHorizontal:  0.33333,
	PageColumns:            6,
	PageRows:               14,
	LabelPaddingHorizontal: 0.0625,
	LabelPaddingVertical:   0.0625,
}
