package label_sheet_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jkmn/barcogen/pkg/label_sheet"
)

func TestLabelSheet_Validate(t *testing.T) {
	for _, ls := range label_sheet.AllSheets {
		a := assert.New(t)
		err := ls.Validate()
		a.NoError(err)
	}
}
