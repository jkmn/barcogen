module gitlab.com/jkmn/barcogen

go 1.17

require (
	github.com/boombuler/barcode v1.0.1
	github.com/phpdave11/gofpdf v1.4.2
	gitlab.com/jkmn/bucket v0.0.0-20160928142243-527f4993c805
	gitlab.com/jkmn/errors v0.0.0-20211020041744-46f7d817a344
)

require github.com/kr/pretty v0.3.1 // indirect

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.8.1
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
