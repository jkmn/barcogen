package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"gitlab.com/jkmn/errors"
)

func loadAssignments(filename string) (ministers, assignments []string, err error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, nil, errors.Stack(err)
	}

	r := csv.NewReader(f)

	ministers = make([]string, 0)
	assignments = make([]string, 0)

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, nil, errors.Stack(err)
		}

		if len(record) != 2 {
			log.Printf("unusual record: %v", record)
			continue
		}

		newMinisters, err := extractMinisters(record[0])
		if err != nil {
			return nil, nil, errors.Stack(err)
		}

		newAssignments, err := extractAssignments(record[1])

		ministers = append(ministers, newMinisters...)
		assignments = append(assignments, newAssignments...)
	}

	return ministers, assignments, nil

}

func extractMinisters(s string) ([]string, error) {
	out := make([]string, 0)
	ministers := strings.Split(s, "\n")
	for _, minister := range ministers {
		minister = strings.TrimSpace(minister)
		if len(minister) > 0 {
			out = append(out, minister)
		}
	}
	return out, nil
}

func extractAssignments(s string) ([]string, error) {
	out := make([]string, 0)
	words := strings.Split(s, " ")
	name := ""
	for _, word := range words {
		word = strings.TrimSpace(word)
		if strings.HasSuffix(word, ",") {
			// We've encountered a surname.
			if name != "" {
				out = append(out, name)
			}
			name = word
		} else {
			// We've encountered some other word (first, middle, or ampersand)
			name = fmt.Sprintf("%s %s", name, word)
		}
	}
	return out, nil
}
