package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"github.com/phpdave11/gofpdf"
	"gitlab.com/jkmn/errors"
)

func loadUnassignedHouseholds(filename string) ([]string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, errors.Stack(err)
	}

	r := csv.NewReader(f)

	households := make([]string, 0)

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, errors.Stack(err)
		}

		if len(record) != 1 {
			log.Printf("unusual record: %v", record)
			continue
		}

		household := strings.TrimSpace(record[0])
		households = append(households, household)
	}

	return households, nil
}

func fillHouseholdCards(c *gofpdf.Creator, households []string) error {
	contents := make([]string, 0)
	for _, household := range households {
		content := fmt.Sprintf("Household: %s", household)
		contents = append(contents, content)
	}
	return fillCards(c, contents)
}
