package main

import (
	"log"
	"sort"

	"github.com/phpdave11/gofpdf"
	"gitlab.com/jkmn/bucket/jlog"
	"gitlab.com/jkmn/errors"
)

var (
	PPI           float64 = gofpdf.PPI // Default points per inch
	PAGE_WIDTH    float64 = PPI * (8.5)
	PAGE_HEIGHT   float64 = PPI * (11.0)
	CARD_HEIGHT   float64 = PPI * (2.0)   // Height of the card
	MARGIN_COL    float64 = PPI * (0)     // Space between the columns of cards
	CARD_PADDING  float64 = PPI * (0.125) // Padding between the content and the card edge
	CARD_WIDTH    float64 = PPI * (3.5)   // Width of the card
	MARGIN_LEFT   float64 = PPI * (0.75)  // Left margin of the card page
	MARGIN_TOP    float64 = PPI * (0.5)   // Top margin of the card page
	CARDS_PER_ROW int     = 2
	CARDS_PER_COL int     = 5
)

func main() {

	var err error

	ministers, households, err := loadAssignments("assignments.csv")
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	unassignedMinisters, err := loadUnassignedMinisters("ministers.csv")
	unassignedHouseholds, err := loadUnassignedHouseholds("ministers.csv")

	ministers = append(ministers, unassignedMinisters...)
	households = append(households, unassignedHouseholds...)

	sort.Strings(ministers)
	sort.Strings(households)

	jlog.Log(ministers)
	jlog.Log(households)

	c := gofpdf.New()
	c.SetPageSize(gofpdf.PageSize{PAGE_WIDTH, PAGE_HEIGHT})
	c.SetPageMargins(0, 0, 0, 0)

	err = fillMinisterCards(c, ministers)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	err = fillHouseholdCards(c, ministers)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	err = c.WriteToFile("out.pdf")
	if err != nil {
		log.Fatal(errors.Stack(err))
	}
}

func fillCards(c *gofpdf.Creator, contents []string) error {
	var err error

	for i, content := range contents {
		// This creates a new page.
		if (i)%10 == 0 {
			c.NewPage()
			drawBorders(c)
		}
		row := (i % (CARDS_PER_ROW * CARDS_PER_COL)) % (CARDS_PER_COL)
		col := (i%(CARDS_PER_ROW*CARDS_PER_COL) + row) % (CARDS_PER_ROW)

		log.Printf("row: %d\tcol: %d", row, col)

		var (
			// These should reflect the top left corner of the physical label.
			x float64 = MARGIN_LEFT + CARD_WIDTH*float64(col) + MARGIN_COL*float64(col)
			y float64 = MARGIN_TOP + CARD_HEIGHT*float64(row)

			// These represent the top left corner of the printable area of the label.
			x_ float64 = x + CARD_PADDING
			y_ float64 = y + CARD_PADDING
		)

		p := c.NewParagraph(content)

		// Finally, draw the paragraph in the proper location.
		c.MoveTo(x_, y_)
		err = c.Draw(p)
		if err != nil {
			log.Fatal(errors.Stack(err))
		}
	}

	return nil
}

func drawBorders(c *gofpdf.Creator) {

	// Draw a lines around cards for debugging
	var (
		x1, x2, y1, y2 float64
		line           gofpdf.Drawable
		err            error
	)

	for col := 0.0; col < 2; col++ {

		// left edge
		x1 = MARGIN_LEFT + col*(CARD_WIDTH+MARGIN_COL)
		x2 = x1
		y1 = 0
		y2 = c.Height()
		line = c.NewLine(x1, y1, x2, y2)
		err = c.Draw(line)
		if err != nil {
			log.Fatal(err)
		}

		// right edge
		x1 = MARGIN_LEFT + col*(CARD_WIDTH+MARGIN_COL) + CARD_WIDTH
		x2 = x1
		y1 = 0
		y2 = c.Height()
		line = c.NewLine(x1, y1, x2, y2)
		err = c.Draw(line)
		if err != nil {
			log.Fatal(errors.Stack(err))
		}
	}

	for y := MARGIN_TOP; y <= c.Height(); y += CARD_HEIGHT {
		x1 = 0
		x2 = c.Width()
		y1 = y
		y2 = y
		line = c.NewLine(x1, y1, x2, y2)
		err = c.Draw(line)
		if err != nil {
			log.Fatal(errors.Stack(err))
		}
	}
}
