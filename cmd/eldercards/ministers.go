package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"github.com/phpdave11/gofpdf"
	"gitlab.com/jkmn/errors"
)

func loadUnassignedMinisters(filename string) ([]string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, errors.Stack(err)
	}

	r := csv.NewReader(f)

	ministers := make([]string, 0)

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, errors.Stack(err)
		}

		if len(record) != 1 {
			log.Printf("unusual record: %v", record)
			continue
		}

		minister := strings.TrimSpace(record[0])
		ministers = append(ministers, minister)
	}

	return ministers, nil
}

func fillMinisterCards(c *gofpdf.Creator, ministers []string) error {
	contents := make([]string, 0)
	for _, minister := range ministers {
		content := fmt.Sprintf("Minister: %s", minister)
		contents = append(contents, content)
	}
	return fillCards(c, contents)
}
